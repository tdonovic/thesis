package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"

	uuid "github.com/google/uuid"
	"github.com/hyperledger/fabric/common/util"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// Set stores the asset (both key and value) on the ledger. If the key exists,
// it will override the value with the new one
func set(stub shim.ChaincodeStubInterface, key string, value QualityEntry) (string, error) {
	if !value.isEmpty() {
		return "", fmt.Errorf("Incorrect arguments. Expecting a filled struct")
	}

	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(value.Data)
	err := stub.PutState(key, []byte(reqBodyBytes.Bytes()))
	if err != nil {
		return "", fmt.Errorf("Failed to set asset: %s", key)
	}
	return string(value.Id), nil
}

// Get returns the value of the specified asset key
func get(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 1 {
		return "", fmt.Errorf("Incorrect arguments. Expecting a key")
	}

	value, err := stub.GetPrivateData("PrivateCollection", args[0])
	if err != nil {
		return "", fmt.Errorf("Failed to get asset: %s with error: %s", args[0], err)
	}
	if value == nil {
		return "", fmt.Errorf("Asset not found: %s", args[0])
	}

	//var qe QualityEntry
	fmt.Print("value\n")
	fmt.Print(string(value))
	//err = json.Unmarshal(value, &qe)
	//if err != nil {
	//	return "", fmt.Errorf("Failed to unmarshal json")
	//}
	return string(value), nil
}

func analyse(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	value, err := stub.GetPrivateData("PrivateCollection", args[0])

	kv := KV{
		"test",
		"value"}

	id, err := uuid.NewUUID()
	uid := id.String()
	fmt.Print(uid)
	var keyValue []KV
	keyValue = append(keyValue, kv)

	//Do analysis here
	hashValue := hash(value)

	entry := QualityEntry{
		Id:   uid,
		Hash: hashValue,
		Data: keyValue}
	js, err := json.Marshal(entry)
	if err != nil {
		return "", err
	}
	fmt.Println("the goods")
	fmt.Println(js)
	err = stub.PutState(args[0], []byte(js))
	if err != nil {
		return "", err
	}
	return uid, nil
}

func importRecords(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	var name string
	name = args[0]
	var csvloc string
	csvloc = args[1]
	fmt.Print(name + "\n")
	fmt.Print(csvloc + "\n")
	if name == "" && csvloc == "" {
		return "", fmt.Errorf("Incorrect arguments. Expecting a name and a csv location")
	}
	csvData, _, err := importCSV(csvloc)
	if err != nil {
		fmt.Print("couldnt import csv")
		return "", err
	}
	js, err := json.Marshal(csvData)
	if err != nil {
		return "", err
	}
	fmt.Println("the goods")
	fmt.Println(js)
	if err != nil {
		return "", err
	}
	chainCodeArgs := util.ToChaincodeArgs("analyse", name)
	response := stub.InvokeChaincode(args[2], chainCodeArgs, args[3])

	if response.Status != shim.OK {
		return "", errors.New(response.Message)
	}
	return string(js), nil
}
