package main

import (
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

// QualityEntry implements a simple chaincode to manage an asset
type QualityEntry struct {
	Id   string `json:"id"`
	Hash string `json:"hash`
	Data []KV   `json:"data"`
}

func (t *QualityEntry) isEmpty() bool {
	if t.Id != "" && t.Hash != "" && len(t.Data) > 0 {
		return true
	}
	return false
}

//KV basic kv obj
type KV struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// Init is called during chaincode instantiation to initialize any
// data. Note that chaincode upgrade also calls this function to reset
// or to migrate data.
func (t *QualityEntry) Init(stub shim.ChaincodeStubInterface) peer.Response {

	return shim.Success(nil)
}

// main function starts up the chaincode in the container during instantiate
func main() {
	if err := shim.Start(new(QualityEntry)); err != nil {
		fmt.Printf("Error starting QualityEntry chaincode: %s", err)
	}
}
