package main

import (
	"bufio"
	"crypto/sha256"
	"encoding/csv"
	"io"
	"os"

	"github.com/btcsuite/btcutil/base58"
)

func importCSV(csvLoc string) ([][]string, string, error) {
	var output [][]string
	f, _ := os.Open(csvLoc)
	reader := csv.NewReader(bufio.NewReader(f))
	for {
		var cline []string
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, "", err
		}
		for _, cell := range line {
			cline = append(cline, string(cell))
		}
		output = append(output, cline)
	}

	f, err := os.Open(csvLoc)
	if err != nil {
		return nil, "", err
	}
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		return nil, "", err
	}

	return output, base58.Encode(h.Sum(nil)), nil
}

func hash(f []byte) string {
	h := sha256.New()
	h.Write(f)

	return base58.Encode(h.Sum(nil))
}
