package main

import (
	"bytes"
	"crypto/sha256"
	b64 "encoding/base64"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/btcsuite/btcutil/base58"
)

func importCSV(csvString string) ([][]string, string, error) {
	var output [][]string
	fmt.Println("Opening CSV from b64 string")
	sDec, _ := b64.StdEncoding.DecodeString(csvString)
	reader := csv.NewReader(bytes.NewReader([]byte(sDec)))
	start := time.Now()

	for {
		var cline []string
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Println("Couldn't read file: " + err.Error())
			return nil, "", err
		}
		for _, cell := range line {
			cline = append(cline, string(cell))
		}
		output = append(output, cline)
	}
	elapsed := time.Since(start)
	log.Printf("Load took %s", elapsed)

	h := sha256.New()
	h.Write([]byte(sDec))

	return output, base58.Encode(h.Sum(nil)), nil
}

func hash(f []byte) string {
	h := sha256.New()
	h.Write(f)

	return base58.Encode(h.Sum(nil))
}
