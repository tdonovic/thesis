package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	uuid "github.com/google/uuid"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// Set stores the asset (both key and value) on the ledger. If the key exists,
// it will override the value with the new one
func set(stub shim.ChaincodeStubInterface, key string, value QualityEntry) (string, error) {
	if !value.isEmpty() {
		return "", fmt.Errorf("Incorrect arguments. Expecting a filled struct")
	}

	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(value.Data)
	err := stub.PutState(key, []byte(reqBodyBytes.Bytes()))
	if err != nil {
		return "", fmt.Errorf("Failed to set asset: %s", key)
	}
	return string(value.Id), nil
}

// Get returns the value of the specified asset key
func get(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 1 {
		return "", fmt.Errorf("Incorrect arguments. Expecting a key")
	}

	value, err := stub.GetPrivateData("collectionPrivate", args[0])
	if err != nil {
		return "", fmt.Errorf("Failed to get asset: %s with error: %s", args[0], err)
	}
	if value == nil {
		return "", fmt.Errorf("Asset not found: %s", args[0])
	}

	//var qe QualityEntry
	fmt.Print("value\n")
	fmt.Print(string(value))
	//err = json.Unmarshal(value, &qe)
	//if err != nil {
	//	return "", fmt.Errorf("Failed to unmarshal json")
	//}
	return string(value), nil
}

func analyse(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	fmt.Println("Getting data from privatedata")
	value, err := stub.GetPrivateData("collectionPrivate", args[0])
	if err != nil {
		fmt.Println("error: " + err.Error())
		return "", err
	}

	id, err := uuid.NewUUID()
	uid := id.String()
	fmt.Print(uid)
	var keyValue []KV

	//Do analysis here
	out, err := missingFields(value)
	keyValue = append(keyValue, out...)
	hashValue := hash(value)

	entry := QualityEntry{
		Id:   uid,
		Hash: hashValue,
		Data: keyValue}
	js, err := json.Marshal(entry)
	if err != nil {
		fmt.Println("Error marshalling json")
		return "", err
	}
	fmt.Println("results")
	fmt.Println(string(js))
	err = stub.PutState("collectionPrivate", []byte(js))
	if err != nil {
		fmt.Println("Error putting state: " + err.Error())
		return "", err
	}
	return uid, nil
}

func importRecords(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	var name string
	name = args[0]
	var csvloc string
	csvloc = args[1]
	fmt.Print("Name of job: " + name + "\n")
	fmt.Print("Name of CSV: " + csvloc + "\n")
	if name == "" && csvloc == "" {
		return "", fmt.Errorf("Incorrect arguments. Expecting a name and a csv location")
	}
	resp, err := http.Get("http://webserver:5000/test.txt")

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	csvData, _, err := importCSV(string(body))
	if err != nil {
		fmt.Println("couldnt import csv: " + err.Error())
		return "", err
	}
	js, err := json.Marshal(csvData)
	if err != nil {
		return "", err
	}
	fmt.Println("the goods")
	fmt.Println(js)
	if err != nil {
		return "", err
	}
	fmt.Println("Putting private data")
	err = stub.PutPrivateData("collectionPrivate", name, js)
	if err != nil {
		fmt.Println("error: " + err.Error())
		return "", err
	}
	fmt.Println("Put and now getting")
	value, err := stub.GetPrivateData("collectionPrivate", name)
	if err != nil {
		fmt.Println("error: " + err.Error())
		return "", err
	}
	fmt.Println("Getting private data back: " + string(value))

	//chainCodeArgs := util.ToChaincodeArgs("analyse", name)
	//fmt.Println("Invoking Chaincode with args: " + args[2] + ", analyse, " + name + ", " + args[3])
	// response := stub.InvokeChaincode(args[2], chainCodeArgs, args[3])
	// fmt.Println("Invocation response:" + string(response.Status) + " " + response.Message)
	// if response.Status != shim.OK {
	// 	fmt.Println("There was an error: " + string(response.Status) + " " + response.Message)
	// 	return "", errors.New(response.Message)
	// }
	return string(js), nil
}
