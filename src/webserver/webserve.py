from flask import Flask
app = Flask(__name__)

@app.route('/<string:file_path>')
def hello_world(file_path):
  f = open("/srv/files/" + file_path, "r")
  return f.read()
