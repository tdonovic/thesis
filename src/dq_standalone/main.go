package main

import (
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

//KV basic kv obj
type KV struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func main() {
	var csvLoc string
	flag.StringVar(&csvLoc, "input", "test.csv", "Path to the csv you want analysed")
	flag.Parse()
	fmt.Println("Starting DQ with following params")
	fmt.Println(csvLoc)
	data := importCsv(csvLoc)
	marshalled, err := json.Marshal(data)
	if err != nil {
		log.Fatalf("Couldn't marshal to json")
	}
	fmt.Println("Calculating missing fields")
	kv, err := missingFields(marshalled)
	if err != nil {
		log.Fatalf("Couldn't calculate missing fields %s", err)
	}
	kvm, err := json.Marshal(kv)
	if err != nil {
		log.Fatalf("Couldn't marshal to json: %s", err)
	}
	fmt.Println("Printing kvm")
	fmt.Println(string(kvm))

}

func importCsv(csvloc string) (data [][]string) {
	f, err := os.Open(csvloc)

	if err != nil {
		log.Fatalf("Cannot open '%s': %s\n", csvloc, err.Error())
	}

	defer f.Close()

	r := csv.NewReader(f)

	r.Comma = ','

	data, err = r.ReadAll()

	if err != nil {
		log.Fatalln("Cannot read CSV data:", err.Error())
	}
	return data
}
