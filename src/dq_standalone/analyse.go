package main

import (
	"encoding/json"
	"fmt"
	"strconv"
)

func missingFields(value []byte) (kv []KV, err error) {
	var data [][]string

	err = json.Unmarshal(value, &data)
	if err != nil {
		//fmt.Println(string(value))
		fmt.Println("Error unmarshalling json: " + err.Error())
		return nil, err
	}
	leng := strconv.Itoa(len(data))
	length := KV{
		"length",
		leng}
	fmt.Sprintf("Data is %d long\n", len(data))
	kv = append(kv, length)

	missing := 0
	expectedlLen := 0

	//If its the header row
	row := data[0]

	fmt.Println(row)
	expectedlLen = len(row)
	fmt.Printf("There are %d columns in this dataset\n", expectedlLen)
	expect := strconv.Itoa(expectedlLen)
	cols := KV{
		"cols",
		string(expect)}
	kv = append(kv, cols)

	for i, row := range data {
		if i != 0 {
			if len(row) != expectedlLen {
				missing++
			} else {
				for _, field := range row {
					miss := 0
					if field == "" {
						miss++
						//Else we add more processing conditions here
					}
					if miss > 0 {
						missing++
					}
				}
			}
		}
	}
	missi := strconv.Itoa(missing)
	fmt.Println("Missing " + string(missi) + "/" + string(leng) + " records.")
	missingKV := KV{
		"missing",
		string(missi)}
	kv = append(kv, missingKV)
	return kv, nil

}
