#!/bin/bash 



#peer chaincode install -n marblesp -v 1.0 -p github.com/chaincode/marbles02_private/go/
peer chaincode install -n tom -v $1 -p github.com/chaincode/tom

export CORE_PEER_ADDRESS=peer1.org1.example.com:8051
peer chaincode install -n tom -v $1 -p github.com/chaincode/tom

echo "[INFO] Setting install env vars for org2"
export CORE_PEER_LOCALMSPID=Org2MSP
export PEER0_ORG2_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ORG2_CA
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp


export CORE_PEER_ADDRESS=peer0.org2.example.com:9051
peer chaincode install -n tom -v $1 -p github.com/chaincode/tom

export CORE_PEER_ADDRESS=peer1.org2.example.com:10051
peer chaincode install -n tom -v $1 -p github.com/chaincode/tom

echo "[INFO] Installed on all peers"



echo "[INFO] Setting instantiate env vars"
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

peer chaincode instantiate -o orderer.example.com:7050 --tls --cafile $ORDERER_CA -C mychannel -n tom -v $1 -c '{"Args":["init"]}' -P "OR('Org1MSP.member','Org2MSP.member')" --collections-config  $GOPATH/src/github.com/chaincode/tom/conf.json

#echo "[INFO] Running tests"
#peer chaincode invoke -o orderer.example.com:7050 --tls --cafile $ORDERER_CA -C mychannel -n tom -c '{"Args":["import","name", "MSwxCjIsMgozLDMK","tom1", "mychannel"]}'



export CORE_PEER_LOCALMSPID=Org1MSP
export PEER0_ORG2_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ORG1_CA
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp

export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
peer chaincode upgrade -o orderer.example.com:7050 --tls --cafile $ORDERER_CA -C mychannel -n marblesp -v 1.1 -c '{"Args":["init"]}' -P "OR('Org1MSP.member','Org2MSP.member')" --collections-config  $GOPATH/src/github.com/chaincode/marbles02_private/collections_config.json
