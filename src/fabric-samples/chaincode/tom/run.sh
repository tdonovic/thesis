#!/bin/bash
echo "[INFO] Setting env vars"
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
echo "[INFO] Invoking Import Chaincode"
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile $ORDERER_CA -C mychannel -n tom -c '{"Args":["import", "collection_element_2", "2k_form.txt"]}'
echo "[INFO] Sleeping for transaction confirmation"
sleep 5
echo "[INFO] Running Analysis"
peer chaincode invoke -o orderer.example.com:7050 --tls --cafile $ORDERER_CA -C mychannel -n tom -c '{"Args":["analyse", "collection_element_2"]}'

