package main

import (
	"bytes"
	"encoding/json"
	"fmt"

	uuid "github.com/google/uuid"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// Set stores the asset (both key and value) on the ledger. If the key exists,
// it will override the value with the new one
func set(stub shim.ChaincodeStubInterface, key string, value QualityEntry) (string, error) {
	if !value.isEmpty() {
		return "", fmt.Errorf("Incorrect arguments. Expecting a filled struct")
	}

	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(value.data)
	err := stub.PutState(key, []byte(reqBodyBytes.Bytes()))
	if err != nil {
		return "", fmt.Errorf("Failed to set asset: %s", key)
	}
	return string(value.id), nil
}

// Get returns the value of the specified asset key
func get(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 1 {
		return "", fmt.Errorf("Incorrect arguments. Expecting a key")
	}

	value, err := stub.GetState(args[0])
	if err != nil {
		return "", fmt.Errorf("Failed to get asset: %s with error: %s", args[0], err)
	}
	if value == nil {
		return "", fmt.Errorf("Asset not found: %s", args[0])
	}
	return string(value), nil
}

func analyse(stub shim.ChaincodeStubInterface, name string, csvloc string) (string, error) {
	if name != "" && csvloc != "" {
		return "", fmt.Errorf("Incorrect arguments. Expecting a name and a csv location")
	}
	_, hash, err := importCSV(csvloc)
	if err != nil {
		return "", err
	}
	//do analysis

	kv := KV{
		"test",
		"value"}
	id, err := uuid.NewUUID()
	uid := id.String()
	var keyValue []KV
	keyValue = append(keyValue, kv)
	entry := QualityEntry{
		id:   uid,
		hash: hash,
		data: keyValue}
	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(entry)
	err = stub.PutState(name, []byte(reqBodyBytes.Bytes()))
	if err != nil {
		return "", err
	}
	return uid, nil
}
